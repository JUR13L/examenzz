<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

/* RUTAS DE LOGIN */
$router->post('/login', 'LoginController@validarusuario');

/* RUTAS DE USUARIOS */
$router->get('/usuarios', 'UsuariosController@index');
$router->get('/usuarios/{id}', 'UsuariosController@getUsuario');
$router->post('/usuarios', 'UsuariosController@createUsuario');
$router->post('/usuarios/{id}', 'UsuariosController@updateUsuario');
$router->delete('/usuarios/{id}', 'UsuariosController@destroyUsuario');

/* RUTAS DE EMPRESAS */
$router->get('/empresas', 'EmpresaController@index');
$router->get('/empresas/{id}', 'EmpresaController@getEmpresa');
$router->post('/empresas', 'EmpresaController@createEmpresa');
$router->post('/empresas/{id}', 'EmpresaController@updateEmpresa');
$router->delete('/empresas/{id}', 'EmpresaController@destroyEmpresa');