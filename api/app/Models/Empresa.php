<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    protected $table = 'empresa';
    protected $primarykey = 'id';
    protected $fillable = ['nombre','rfc','domicilio','municipio','estado','cp','activa'];
}