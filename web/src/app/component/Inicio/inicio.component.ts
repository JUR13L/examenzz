import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'inicio',
    templateUrl: './inicio.component.html',
    styleUrls: ['./inicio.component.css']
})

export class InicioComponent {

    public titulo: string;
    public user: any;
    public estatus: any;

    constructor(
        private _route: ActivatedRoute,
        private _router: Router
    ) {
        this.titulo = 'Inicio';
        this.user = localStorage.getItem('Login');
        this.estatus = localStorage.getItem('estatus');
        //alert(this.user+','+this.estatus);
    }

    ngOnInit() {
        if (!localStorage.getItem('estatus')) {
            this._router.navigate(['login']);
        }
    }

    cerrarsesion() {
        localStorage.removeItem('Login');
        localStorage.removeItem('estatus');
        this._router.navigate(['login']);
    }
}