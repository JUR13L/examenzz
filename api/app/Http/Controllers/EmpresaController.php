<?php

namespace App\Http\Controllers;

use App\Models\Empresa;
use Illuminate\Http\Request;

class EmpresaController extends Controller
{

    /* LISTAR TODOS LOS REGISTROS */
    public function index()
    {
        $empresas = Empresa::all();

        if(count($empresas) > 0){
            return response()->json(['status' => 'OK', 'data' => $empresas, 'Mensaje' => 'Registros de empresas'],200);
        }
        else{
            return response()->json(['status' => 'OK', 'data' => array(), 'Mensaje' => 'Sin registros de empresas'],200);
        }
    }
    /* OBTENER UN REGISTROS ESPECIFICO */
    public function getEmpresa($id)
    {
        $empresa = Empresa::find($id);

        if($empresa){
            return response()->json(['status' => 'OK', 'data' => $empresa, 'Mensaje' => 'Registro de empresa'],200); 
        }
        else{
            return response()->json(['status' => 'OK', 'data' => array(), 'Mensaje' => 'No existe registro de esa empresa'],200);
        }
    }
    /* CREAR REGISTRO NUEVO */
    public function createEmpresa(Request $request)
    {
        $empresa = new Empresa;
        $empresa->nombre = $request->nombre;
        $empresa->rfc = $request->rfc;
        $empresa->domicilio = $request->domicilio;
        $empresa->municipio = $request->municipio;
        $empresa->estado = $request->estado;
        $empresa->cp = $request->cp;
        $empresa->save();
        if($empresa){
            return response()->json(['status' => 'OK', 'data' => array(), 'Mensaje' => 'Registro guardado'],200); 
        }
        else{
            return response()->json(['status' => 'FAIL', 'data' => array(), 'Mensaje' => 'No se registro la empresa, vuelva a intentarlo'],200); 
        }
    }
    /* ACTUALIZAR REGISTRO ESPECIFICO */
    public function updateEmpresa($id,Request $request)
    {
        $empresa = Empresa::find($id);
        $empresa->nombre = $request->nombre;
        $empresa->rfc = $request->rfc;
        $empresa->domicilio = $request->domicilio;
        $empresa->municipio = $request->municipio;
        $empresa->estado = $request->estado;
        $empresa->cp = $request->cp;
        $empresa->activa = $request->activa;
        $empresa->save();
        if($empresa){
            return response()->json(['status' => 'OK', 'data' => $empresa, 'Mensaje' => 'Registro actualizado'],200); 
        }
        else{
            return response()->json(['status' => 'FAIL', 'data' => array(), 'Mensaje' => 'No se actaulizo la empresa, vuelva a intentarlo'],200); 
        }
    }
    /* ELIMINAR REGISTRO ESPECIFICO */
    public function destroyEmpresa($id)
    {
        $empresa = Empresa::find($id);
        $empresa->delete();
        if($empresa){
            return response()->json(['status' => 'OK', 'data' => array(), 'Mensaje' => 'Registro eliminado'],200); 
        }
        else{
            return response()->json(['status' => 'OK', 'data' => array(), 'Mensaje' => 'No se elimino la empresa, vuelva a intentarlo'],200); 
        }
    }
}