import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgZorroAntdModule, NZ_I18N, es_ES } from 'ng-zorro-antd';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpModule } from '@angular/http';
import { registerLocaleData } from '@angular/common';
import es from '@angular/common/locales/es';

import { ErrorComponent } from './component/Error/error.component';
import { LoginComponent } from './component/Login/login.component';
import { InicioComponent } from './component/Inicio/inicio.component';
import { EmpresaComponent } from './component/Empresa/empresa.component';
import { UsuarioComponent } from './component/Usuarios/usuario.component';

registerLocaleData(es);

@NgModule({
  declarations: [
    AppComponent,
    ErrorComponent,
    LoginComponent,
    InicioComponent,
    EmpresaComponent,
    UsuarioComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgZorroAntdModule,
    FormsModule,
    HttpClientModule,
    HttpModule,
    BrowserAnimationsModule
  ],
  providers: [{ provide: NZ_I18N, useValue: es_ES }],
  bootstrap: [AppComponent]
})
export class AppModule { }
