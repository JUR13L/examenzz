import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ErrorComponent } from './component/Error/error.component';
import { LoginComponent } from './component/Login/login.component';
import { InicioComponent } from './component/Inicio/inicio.component';
import { EmpresaComponent } from './component/Empresa/empresa.component';
import { UsuarioComponent } from './component/Usuarios/usuario.component';


const routes: Routes = [
  {path: '', component: LoginComponent },
  {path: 'login', component: LoginComponent},
  {path: 'inicio', component: InicioComponent},
  {path: 'empresa', component: EmpresaComponent},
  {path: 'usuarios', component: UsuarioComponent},
  {path: '**', component: ErrorComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
