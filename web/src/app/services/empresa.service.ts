import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Empresa } from '../models/Empresa';

@Injectable()

export class EmpresaService{
    public url:string;
    
    constructor(
        public _http: Http
    ){
        this.url='http://localhost:8000';
    }

    getEmpresas(){
        //return "TEXTO DESDE EL SERVICIO";
        return this._http.get(this.url+'/empresas').map(res => res.json());
    }

    addEmpresa(empresa: Empresa){
        /* let json = JSON.stringify(producto);
        let params = 'json='+json;
        let headers = new Headers({'Content-Type':'application/form-data'}); */

        /* return this._http.post(this.url+'/productos',params, {headers: headers})
                    .map(res => res.json()); */
        return this._http.post(this.url+'/empresas', empresa).map(res => res.json());

    }

    updateEmpresa(empresa: Empresa){
        /* let json = JSON.stringify(producto);
        let params = 'json='+json;
        let headers = new Headers({'Content-Type':'application/form-data'}); */

        /* return this._http.post(this.url+'/productos',params, {headers: headers})
                    .map(res => res.json()); */
        return this._http.post(this.url+'/empresas/'+empresa.id, empresa).map(res => res.json());

    }

    deleteEmpresa(id){
        return this._http.delete(this.url+'/empresas/'+id).map(res => res.json());
    }
}