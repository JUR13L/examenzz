import { Component } from '@angular/core';
import { LoginService } from '../../services/login.service';
import { Usuario } from 'src/app/models/Usuario';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css'],
    providers: [LoginService]
})

export class LoginComponent{
    public titulo:string;
    public formulario: Usuario; 
    public user:any;
    public estatus:any;

    constructor(
        private _loginServices: LoginService,
        private _route: ActivatedRoute,
        private _router: Router
    ){
        this.titulo='Login';
        this.formulario = new Usuario(0,"","",0,"","");
    }

    ngOnInit(){
    }

    onSubmit(){
        //console.log(this.formulario);

        this._loginServices.getLogeo(this.formulario).subscribe(
            response =>{
                if(response.status == 'OK'){
                    console.log(response.data['Usuario']);
                    if(response.data['Usuario'] != 'S/R'){
                        this.formulario = new Usuario(0,"","",0,"","");
                        localStorage.setItem('Login', response.data['Usuario']);
                        localStorage.setItem('estatus', '1');
                        this.user=localStorage.getItem('Login');
                        this.estatus=localStorage.getItem('estatus');
                        alert('Bienvenido');
                        this._router.navigate(['inicio']);
                        //alert(this.user+', '+this.estatus);

                    }
                    else{
                        this.formulario = new Usuario(0,"","",0,"","");
                        alert('Usuario y/o password incorrecto');
                    }
                    
                    //localStorage.getItem('User',response.data);
                    /* this.ngOnInit();
                    this.handleOk(); */
                }
                else{
                    console.log(response);
                }
            },
            error => {
                console.log(<any>error);
            }

        );
    }
}