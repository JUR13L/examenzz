<?php

namespace App\Http\Controllers;

use App\Models\Usuarios;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function validarusuario(Request $request)
    {
        $usuario = Usuarios::where([
            ['login','=',$request->login],
            ['password','=',$request->password],
            ['activo','=',1]
        ])->get();
        if(count($usuario) > 0){
            return response()->json(['status' => 'OK', 'data' => array("Usuario"=>$usuario[0]["login"]), 'Mensaje' => 'Usuario validado'],200); 
        }
        else{
            return response()->json(['status' => 'OK', 'data' => array("Usuario"=>"S/R"), 'Mensaje' => 'Usuario y/o contraseña no valido'],200); 
        }
    }
}