import { Component } from '@angular/core';
import { UsuarioService } from '../../services/usuario.service';
import { Usuario } from 'src/app/models/Usuario';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'usuario',
    templateUrl: './usuario.component.html',
    providers: [UsuarioService]
})

export class UsuarioComponent {
    public titulo: string;
    public user: any;
    public estatus: any;

    public usuario: Usuario;
    public usuarios: Usuario[];
    /* MODAL */
    isVisible = false;
    isVisibleM = false;
    //isOkLoading = false;
    /* FORMULARIO */
    public formulario: Usuario;

    constructor(
        private _usuarioServices: UsuarioService,
        private _route: ActivatedRoute,
        private _router: Router
    ) {
        this.titulo = 'Usuarios';
        this.user = localStorage.getItem('Login');
        this.estatus = localStorage.getItem('estatus');
        this.formulario = new Usuario(0, "", "", 0, "", "");

    }

    cerrarsesion() {
        localStorage.removeItem('Login');
        localStorage.removeItem('estatus');
        this._router.navigate(['login']);
    }

    showModal(): void {
        this.isVisible = true;
    }
    showModalM(): void {
        this.isVisibleM = true;
    }

    handleOk(): void {
        //this.isOkLoading = true;
        //setTimeout(() => {
        this.isVisible = false;
        this.formulario = new Usuario(0, "", "", 0, "", "");
        //this.isOkLoading = false;
        //}, 3000);

        //alert('Datos, nombre: '+this.formulario.nombre+', descripción: '+this.formulario.descripion+', precio: '+this.formulario.precio+', imagen: '+this.formulario.imagen);
    }

    handleOkM(): void {
        //this.isOkLoading = true;
        //setTimeout(() => {
        this.isVisibleM = false;
        this.formulario = new Usuario(0, "", "", 0, "", "");
        //this.isOkLoading = false;
        //}, 3000);

        //alert('Datos, nombre: '+this.formulario.nombre+', descripción: '+this.formulario.descripion+', precio: '+this.formulario.precio+', imagen: '+this.formulario.imagen);
    }

    handleCancel(): void {
        console.log('Button cancel clicked!');
        this.isVisible = false;
        this.formulario = new Usuario(0, "", "", 0, "", "");
    }

    handleCancelM(): void {
        console.log('Button cancel clicked!');
        this.isVisibleM = false;
        this.formulario = new Usuario(0, "", "", 0, "", "");
    }

    ngOnInit() {
        //alert(this._productosservices.getProductos());
        if (localStorage.getItem('estatus')) {
            this._usuarioServices.getUsuarios().subscribe(
                result => {
                    //console.log(result);
                    if (result.code == 200) {
                        console.log(result);
                    }
                    else {
                        this.usuarios = result.data;
                        this.formulario = new Usuario(0, "", "", 0, "", "");
                    }
                },
                error => {
                    console.log(<any>error);
                }
            );
        }
        else{
            this._router.navigate(['login']);
        }


    }

    onSubmit() {
        console.log(this.formulario);
        //console.log(this.fileToUpload);
        //alert('Datos, nombre: '+this.formulario.nombre+', descripción: '+this.formulario.descripion+', precio: '+this.formulario.precio+', imagen: '+this.formulario.imagen);

        this._usuarioServices.addUsuario(this.formulario).subscribe(
            response => {
                if (response.status == 'OK') {
                    alert('Registro guardado');
                    this.ngOnInit();
                    this.handleOk();
                }
                else {
                    console.log(response)
                }
            },
            error => {
                console.log(<any>error);
            }

        );
    }

    editar(id, nombre, login, activo, perfil, password) {
        console.log('editar: ' + id);
        this.formulario = new Usuario(id, nombre, login, activo, perfil, password);
        console.log(this.formulario);
        this.showModalM();
    }

    actualizar() {
        console.log(this.formulario);
        this._usuarioServices.updateUsuario(this.formulario).subscribe(
            response => {
                if (response.status == 'OK') {
                    alert('Registro actualizado');
                    this.ngOnInit();
                    this.handleOkM();
                }
                else {
                    console.log(response)
                }
            },
            error => {
                console.log(<any>error);
            }

        );
    }

    eliminar(id) {
        console.log('eliminar: ' + id);
        this._usuarioServices.deleteUsuario(id).subscribe(
            response => {
                if (response.status == 'OK') {
                    this.ngOnInit();
                    alert('Registro eliminado');
                }
                else {
                    console.log(response)
                }
            },
            error => {
                console.log(<any>error);
            }

        );
    }
}