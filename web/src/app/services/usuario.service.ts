import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Usuario } from '../models/Usuario';

@Injectable()

export class UsuarioService{
    public url:string;
    
    constructor(
        public _http: Http
    ){
        this.url='http://localhost:8000';
    }

    getUsuarios(){
        //return "TEXTO DESDE EL SERVICIO";
        return this._http.get(this.url+'/usuarios').map(res => res.json());
    }

    addUsuario(usuario: Usuario){
        /* let json = JSON.stringify(producto);
        let params = 'json='+json;
        let headers = new Headers({'Content-Type':'application/form-data'}); */

        /* return this._http.post(this.url+'/productos',params, {headers: headers})
                    .map(res => res.json()); */
        return this._http.post(this.url+'/usarios', usuario).map(res => res.json());

    }

    updateUsuario(usuario: Usuario){
        /* let json = JSON.stringify(producto);
        let params = 'json='+json;
        let headers = new Headers({'Content-Type':'application/form-data'}); */

        /* return this._http.post(this.url+'/productos',params, {headers: headers})
                    .map(res => res.json()); */
        return this._http.post(this.url+'/usuarios/'+usuario.id, usuario).map(res => res.json());

    }

    deleteUsuario(id){
        return this._http.delete(this.url+'/usuarios/'+id).map(res => res.json());
    }
}