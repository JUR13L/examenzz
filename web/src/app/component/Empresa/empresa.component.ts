import { Component } from '@angular/core';
import { EmpresaService } from '../../services/empresa.service';
import { Empresa } from 'src/app/models/Empresa';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'empresa',
    templateUrl: './empresa.component.html',
    providers: [EmpresaService]
})

export class EmpresaComponent {
    public titulo: string;
    public user: any;
    public estatus: any;

    public empresa: Empresa;
    public empresas: Empresa[];
    /* MODAL */
    isVisible = false;
    isVisibleM = false;
    //isOkLoading = false;
    /* FORMULARIO */
    public formulario: Empresa;

    constructor(
        private _empresasServices: EmpresaService,
        private _route: ActivatedRoute,
        private _router: Router
    ) {
        this.titulo = 'Empresas';
        this.user = localStorage.getItem('Login');
        this.estatus = localStorage.getItem('estatus');
        this.formulario = new Empresa(0, "", "", "", "", "", "", 0);

    }

    cerrarsesion() {
        localStorage.removeItem('Login');
        localStorage.removeItem('estatus');
        this._router.navigate(['login']);
    }

    showModal(): void {
        this.isVisible = true;
    }
    showModalM(): void {
        this.isVisibleM = true;
    }

    handleOk(): void {
        //this.isOkLoading = true;
        //setTimeout(() => {
        this.isVisible = false;
        this.formulario = new Empresa(0, "", "", "", "", "", "", 0);
        //this.isOkLoading = false;
        //}, 3000);

        //alert('Datos, nombre: '+this.formulario.nombre+', descripción: '+this.formulario.descripion+', precio: '+this.formulario.precio+', imagen: '+this.formulario.imagen);
    }
    handleOkM(): void {
        //this.isOkLoading = true;
        //setTimeout(() => {
        this.isVisibleM = false;
        this.formulario = new Empresa(0, "", "", "", "", "", "", 0);
        //this.isOkLoading = false;
        //}, 3000);

        //alert('Datos, nombre: '+this.formulario.nombre+', descripción: '+this.formulario.descripion+', precio: '+this.formulario.precio+', imagen: '+this.formulario.imagen);
    }

    handleCancel(): void {
        console.log('Button cancel clicked!');
        this.isVisible = false;
        this.formulario = new Empresa(0, "", "", "", "", "", "", 0);
    }
    handleCancelM(): void {
        console.log('Button cancel clicked!');
        this.isVisibleM = false;
        this.formulario = new Empresa(0, "", "", "", "", "", "", 0);
    }

    ngOnInit() {
        if (localStorage.getItem('estatus')) {
            //alert(this._productosservices.getProductos());
            this._empresasServices.getEmpresas().subscribe(
                result => {
                    //console.log(result);
                    if (result.code == 200) {
                        console.log(result);
                    }
                    else {
                        this.empresas = result.data;
                        this.formulario = new Empresa(0, "", "", "", "", "", "", 0);
                    }
                },
                error => {
                    console.log(<any>error);
                }
            );
        }
        else {
            this._router.navigate(['login']);
        }

    }

    onSubmit() {
        console.log(this.formulario);
        //console.log(this.fileToUpload);
        //alert('Datos, nombre: '+this.formulario.nombre+', descripción: '+this.formulario.descripion+', precio: '+this.formulario.precio+', imagen: '+this.formulario.imagen);

        this._empresasServices.addEmpresa(this.formulario).subscribe(
            response => {
                if (response.status == 'OK') {
                    alert('Registro guardado');
                    this.ngOnInit();
                    this.handleOk();
                }
                else {
                    console.log(response)
                }
            },
            error => {
                console.log(<any>error);
            }

        );
    }

    editar(id, nombre, rfc, domicilio, municipio, estado, cp, activa) {
        console.log('editar: ' + id);
        this.formulario = new Empresa(id, nombre, rfc, domicilio, municipio, estado, cp, activa);
        console.log(this.formulario);
        this.showModalM();
    }

    actualizar() {
        console.log(this.formulario);
        this._empresasServices.updateEmpresa(this.formulario).subscribe(
            response => {
                if (response.status == 'OK') {
                    alert('Registro actualizado');
                    this.ngOnInit();
                    this.handleOkM();
                }
                else {
                    console.log(response)
                }
            },
            error => {
                console.log(<any>error);
            }

        );
    }

    eliminar(id) {
        console.log('eliminar: ' + id);
        this._empresasServices.deleteEmpresa(id).subscribe(
            response => {
                if (response.status == 'OK') {
                    this.ngOnInit();
                    alert('Registro eliminado');
                }
                else {
                    console.log(response)
                }
            },
            error => {
                console.log(<any>error);
            }

        );
    }
}