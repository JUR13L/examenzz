import { Component } from '@angular/core';
import { LiteralExpr } from '@angular/compiler';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public estatus:any;
  public title:string;

  constructor(){
    this.title = 'web';
    if(localStorage.getItem('estatus')){
      this.estatus = localStorage.getItem('estatus');
    }
    else{
      this.estatus=0;
    }

  }
}
