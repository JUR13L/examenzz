export class Empresa{
    constructor(
        public id:number,
        public nombre:string,
        public rfc:string,
        public domicilio:string,
        public municipio:string,
        public estado:string,
        public cp:string,
        public activa:number        
    ){}
}