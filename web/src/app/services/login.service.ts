import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Usuario } from '../models/Usuario';

@Injectable()

export class LoginService{
    public url:string;
    
    constructor(
        public _http: Http
    ){
        this.url='http://localhost:8000';
    }

    getLogeo(usuario: Usuario){
        return this._http.post(this.url+'/login', usuario).map(res => res.json());
    }

    /* getProductos(){
        //return "TEXTO DESDE EL SERVICIO";
        return this._http.get(this.url+'/productos').map(res => res.json());
    } */

    /*addProductos(usuario: Usuario){
        /* let json = JSON.stringify(producto);
        let params = 'json='+json;
        let headers = new Headers({'Content-Type':'application/form-data'}); */

        /* return this._http.post(this.url+'/productos',params, {headers: headers})
                    .map(res => res.json()); * /
        return this._http.post(this.url+'/productos', usuario).map(res => res.json());

    }

    deleteProductos(id){
        return this._http.delete(this.url+'/productos/'+id).map(res => res.json());
    } */
}