<?php

namespace App\Http\Controllers;

use App\Models\Usuarios;
use Illuminate\Http\Request;

class UsuariosController extends Controller
{

    /* LISTAR TODOS LOS REGISTROS */
    public function index()
    {
        $usuarios = Usuarios::all();

        if(count($usuarios) > 0){
            return response()->json(['status' => 'OK', 'data' => $usuarios, 'Mensaje' => 'Registros de usuarios'],200);
        }
        else{
            return response()->json(['status' => 'OK', 'data' => array(), 'Mensaje' => 'Sin registros de usuarios'],200);
        }
    }
    /* OBTENER UN REGISTROS ESPECIFICO */
    public function getUsuario($id)
    {
        $usuario = Usuarios::find($id);

        if($usuario){
            return response()->json(['status' => 'OK', 'data' => $usuario, 'Mensaje' => 'Registro de usuario'],200); 
        }
        else{
            return response()->json(['status' => 'OK', 'data' => array(), 'Mensaje' => 'No existe registro de ese usuario'],200);
        }
    }
    /* CREAR REGISTRO NUEVO */
    public function createUsuario(Request $request)
    {
        $usuario = new Usuarios;
        $usuario->nombre = $request->nombre;
        $usuario->login = $request->login;
        if(!empty($request->perfil)){
            $usuario->perfil = $request->perfil;
        }
        $usuario->password = $request->password;
        $usuario->save();
        if($usuario){
            return response()->json(['status' => 'OK', 'data' => array(), 'Mensaje' => 'Registro guardado'],200); 
        }
        else{
            return response()->json(['status' => 'OK', 'data' => array(), 'Mensaje' => 'No se registro el usuario, vuelva a intentarlo'],200); 
        }
    }
    /* ACTUALIZAR REGISTRO ESPECIFICO */
    public function updateUsuario(Request $request,$id)
    {
        $usuario = Usuarios::find($id);
        $usuario->nombre = $request->nombre;
        $usuario->login = $request->login;
        $usuario->perfil = $request->perfil;
        $usuario->activo = $request->activo;
        $usuario->password = $request->password;
        $usuario->save();
        if($usuario){
            return response()->json(['status' => 'OK', 'data' => $usuario, 'Mensaje' => 'Registro actualizado'],200); 
        }
        else{
            return response()->json(['status' => 'OK', 'data' => array(), 'Mensaje' => 'No se actaulizo el usuario, vuelva a intentarlo'],200); 
        }
    }
    /* ELIMINAR REGISTRO ESPECIFICO */
    public function destroyUsuario($id)
    {
        $usuario = Usuarios::find($id);
        $usuario->delete();
        if($usuario){
            return response()->json(['status' => 'OK', 'data' => array(), 'Mensaje' => 'Registro eliminado'],200); 
        }
        else{
            return response()->json(['status' => 'OK', 'data' => array(), 'Mensaje' => 'No se elimino el usuario, vuelva a intentarlo'],200); 
        }
    }
}