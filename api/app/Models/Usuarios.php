<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Usuarios extends Model
{
    protected $talbe = 'usuarios';
    protected $primaryKey = 'id';
    protected $fillable = ['nombre','login','activo','perfil','password'];
}