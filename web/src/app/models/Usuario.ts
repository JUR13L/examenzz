export class Usuario{
    constructor(
        public id:number,
        public nombre:string,
        public login:string,
        public activo:number,
        public perfil:string,
        public password:string
    ){}
}