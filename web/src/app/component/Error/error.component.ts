import { Component } from "@angular/core";
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'error',
    templateUrl : './error.component.html',
    styleUrls: ['./error.component.css']
})
export class ErrorComponent {

    constructor(
        private _route: ActivatedRoute,
        private _router: Router
    ){
    }

    ngOnInit(){
        if (!localStorage.getItem('estatus')) {
            this._router.navigate(['login']);
        }
    }

    cerrarsesion(){
        localStorage.removeItem('Login');
        localStorage.removeItem('estatus');
        this._router.navigate(['login']);
    }
    
}
